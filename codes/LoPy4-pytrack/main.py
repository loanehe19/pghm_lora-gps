## Test code for read date/time from gps and update RTC
import machine
import math
import network
import os
import time
import utime
import pycom
import binascii
import struct
import socket
import cayenneLPP           # add CAYENNE LPP FORMAT

from machine import RTC
from machine import SD
from network import LoRa, Bluetooth, Sigfox
from machine import Timer
from L76GNSS import L76GNSS
from pytrack import Pytrack
from machine import Pin
from OTA import WiFiOTA
from time import sleep

import struct
# setup as a station

import gc

#tableau de couleurs pour la led
colorled= ['0x007f00', '0x7f7f00', '0x7f0000','0xB805AE','0xE29E32']

GPS = 0
cayenne = 1
Bluetooth_to_LoRa = 1
uSD = 0


#numéro de la carte Pycom (BOUSIN)
#sert essentiellement au nom Bluetooth
bousin='BOUSIN 12'

global BLUETOOTH_DATA

pycom.heartbeat(False)

time.sleep(2)
gc.enable()

#Start GPS
py = Pytrack()
#l76 = L76GNSS(py, timeout=600)

pycom.rgbled(0x001111)

if GPS == 1 :
    #start rtc
    rtc = machine.RTC()
    print('Aquiring GPS signal....')
    #try to get gps date to config rtc
    while (True):
       gps_datetime = l76.get_datetime()
       #case valid readings
       if gps_datetime[3]:
           day = int(gps_datetime[4][0] + gps_datetime[4][1] )
           month = int(gps_datetime[4][2] + gps_datetime[4][3] )
           year = int('20' + gps_datetime[4][4] + gps_datetime[4][5] )
           hour = int(gps_datetime[2][0] + gps_datetime[2][1] )
           minute = int(gps_datetime[2][2] + gps_datetime[2][3] )
           second = int(gps_datetime[2][4] + gps_datetime[2][5] )
           print("Current location: {}  {} ; Date: {}/{}/{} ; Time: {}:{}:{}".format(gps_datetime[0],gps_datetime[1], day, month, year, hour, minute, second))
           rtc.init( (year, month, day, hour, minute, second, 0, 0))
           break

    print('RTC Set from GPS to UTC:', rtc.now())

    chrono = Timer.Chrono()
    chrono.start()

if uSD == 1 :
    # CARTE µSD
    sd = SD()
    os.mount(sd, '/sd')
    f = open('/sd/gps-record.txt', 'w')

# -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
# Initialize Bluetooth.
def init_BLE():
    print('Activation Bluetooth')
    global bt_periph
    bt_periph = Bluetooth()
    bt_periph.set_advertisement(name = bousin, service_uuid = b'1234567890123456')
    pycom.rgbled(0x00007f)  # Blue

def init_BLE_communication(bt_periph):
    bt_periph.callback(trigger=Bluetooth.CLIENT_CONNECTED | Bluetooth.CLIENT_DISCONNECTED, handler=conn_cb)
    bt_periph.advertise(True)
    #Update the characteristic value very few seconds
    srv1 = bt_periph.service(uuid='0000000000000000', isprimary=True)
    chr1 = srv1.characteristic(uuid='0000000000000002', properties=Bluetooth.PROP_INDICATE | Bluetooth.PROP_BROADCAST | Bluetooth.PROP_READ | Bluetooth.PROP_NOTIFY | Bluetooth.PROP_WRITE, value='InitialValue')
    chr1.callback(trigger=Bluetooth.CHAR_READ_EVENT | Bluetooth.CHAR_WRITE_EVENT, handler=char1_cb)
    return chr1

def conn_cb (bt_o):
    events = bt_o.events()
    if  events & Bluetooth.CLIENT_CONNECTED:
        print("Client connected")
    elif events & Bluetooth.CLIENT_DISCONNECTED:
        print("Client disconnected")

def char1_cb(chr):
    events = chr.events()
    if events & Bluetooth.CHAR_READ_EVENT :
        print('Bluetooth.CHAR_READ_EVENT')
    elif events & Bluetooth.CHAR_WRITE_EVENT:
        print("Write request with value = {}".format(chr.value()))
        print(chr.value())
        BLUETOOTH_DATA = chr.value()
        if(chr.value()==b'quit'):
            #deconnecter le bluetooth
            bt_periph.disconnect_client()
            bt_periph.advertise(False)
            print("Client disconnected")
            pycom.rgbled(0x7f0000)
        if((Bluetooth_to_LoRa == 1) and (chr.value()!=b'quit') and (lora.has_joined())):
            # send some data
            s.send(bytes(BLUETOOTH_DATA))
        if(chr.value()==b'\x01'):
            pycom.rgbled(0x110000)
        if(chr.value()==b'\x02'):
            pycom.rgbled(0x001100)
        if(chr.value()==b'\x03'):
            pycom.rgbled(0x000011)
        if(chr.value()==b'\x04'):
            pycom.rgbled(0x110011)  #magenta
        if(chr.value()==b'\x05'):
            pycom.rgbled(0x001111)  #cyan
        if(chr.value()==b'\x06'):
            pycom.rgbled(0x111100)  #jaune
        if(chr.value()==b'\x07'):
            pycom.rgbled(0x111111)





def BLE_send(chr,val):
    print('Setting value: {}'.format(val))
    chr.value(val)

init_BLE()
c=init_BLE_communication(bt_periph)

# -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
# Initialize LoRa in LORAWAN mode.
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)

app_eui = binascii.unhexlify('70B3D57ED0028E0B')
app_key = binascii.unhexlify('3F55252B112175FC24A48E89AC5E333B')

# join a network using OTAA (Over the Air Activation)
lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)

# wait until the module has joined the network
while not lora.has_joined():
    time.sleep(2.5)
    print('Not yet joined...')

print('joined !!')
pycom.rgbled(0x777700)

# create a LoRa socket
s = socket.socket(socket.AF_LORA, socket.SOCK_RAW)

# set the LoRaWAN data rate
s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)

# make the socket blocking
# (waits for the data to be sent and for the 2 receive windows to expire)
s.setblocking(True)

while (True):
   volt_float = (py.read_battery_voltage()*100)/4.5     # Batterie
   Batt=int(volt_float)
  # print("RTC time : {}".format(rtc.now()))
  # coord = l76.coordinates()
 #  print("$GPGLL>> {} - Free Mem: {}".format(coord, gc.mem_free()))
#   coord1 = l76.coordinates1()
#   print("$GPGGA>> {} - Free Mem: {}".format(coord1, gc.mem_free()))
#   coord2 = l76.get_datetime()
#   print("$G_RMC>> {} - Free Mem: {}".format(coord2, gc.mem_free()))
 #  print("longitude : {} - latitude {} - altitude {} -  Satellites visibles {}      -    Batterie = {}".format(coord1[0],coord1[1],coord1[3],coord1[5],Batt));
 #  if uSD == 1 :
#       f.write("{} - {} - {}\n".format(coord1, rtc.now(), Batt))
   # send some data
   # s.send(coord1[0],coord1[1],coord1[3])      /!\ MODIFIER

   if cayenne == 1:
       # creating Cayenne LPP packet
       lpp = cayenneLPP.CayenneLPP(size = 100, sock = s)
       lpp.add_analog_input(Batt,1)     # Batterie LPP Cayene

       if GPS == 1 :
           if (coord1[0] != None) or (coord1[1] != None) or (coord1[3] != None) or (coord1[5] != None):
               SatGPS = int(coord1[5]) + 0
               lpp.add_analog_input(SatGPS,2)     # Nombre de Satellites

               AltGPS = float(coord1[3]) + 0.00     # Altitude du GPS
               lpp.add_gps(coord1[0], coord1[1], AltGPS)
               lpp.send(reset_payload = True)

       # sending the packet via the socket
       lpp.send()

   # make the socket non-blocking
   # (because if there's no data received it will block forever...)
   s.setblocking(False)

   # get any data received (if any...)
   dataReceived = s.recv(64)
   print(dataReceived)

   # Some sort of OTA trigger
   if dataReceived == bytes([0x01, 0x02, 0x03]):
       pycom.rgbled(0xffffff)
       print("Performing OTA")
       # Perform OTA
       ota.connect()
       ota.update()

   if dataReceived == bytes([0x03, 0x03, 0x03]):        #Mode Sigfox
       pycom.rgbled(int(colorled[4]))
       print('Connexion a Sigfox')
       # init Sigfox for RCZ1 (Europe)
       sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)
       # create a Sigfox socket
       s = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)
       # make the socket blocking
       s.setblocking(True)
       # configure it as uplink only
       s.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)
       #s.send(bytes([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]))
       s.send(bytes([(int(message[0:2],16)),(int(message[2:4],16)), (int(message[4:6],16)),(int(message[6:8],16)),(int(message[8:10],16)),(int(message[10:12],16)),(int(message[12:14],16)),(int(message[14:16],16)),(int(message[16:18],16)),(int(message[18:20],16)),(int(message[20:22],16)),(int(message[22:24],16))]))
       s.setblocking(False)
       s.close()
       print('Message SIGFOX envoye')
       pycom.rgbled(int(colorled[2])) # red

   if dataReceived == bytes([0x06, 0x06, 0x06]):
       pycom.rgbled(0xff0000)

   if dataReceived == bytes([0x88, 0x88, 0x88]):
       pycom.rgbled(0xff00ff)


   sleep(5)
