#programme BOUSIN cas terrain - homme
#par dÃÂ©faut, le BOUSIN se rÃÂ©veille tous les jours pour montrer son existence en central et envoyer son niveau de Batterie.
#il fait un cycle classic et envoyant en LoRa et en SIGFOX
#il est possible de rÃÂ©veiller le BOUSIN en le secouant assez fort pendant 10s
#Autre action possible : l'appui sur le bouton d'alerte.
#Dans ce cas,
#1ÃÂ°) le BOUSIN active son Bluetooth pour ÃÂªtre dÃÂ©tectÃÂ© par un tÃÂ©lÃÂ©phone secdroid ayant l'application alerte en route
#2ÃÂ°) envoi le signal d'alerte sur le rÃÂ©seau LoRa
#3ÃÂ°) coupe le bluetooth (aprÃÂ¨s une dizaine de secondes activÃÂ©, plus d'intÃÂ©rÃÂªt si le secdroid n'a pas dÃÂ©tectÃÂ© le signal)
#4ÃÂ°) envoi le signal d'alerte sur le rÃÂ©seau SigFox
#5ÃÂ°) recherche du signal gps
#6ÃÂ°) envoi des coordonnÃÂ©es via LoRa et SigFox
#7ÃÂ°) Remise en sommeil du BOUSIN
from pytrack import Pytrack
from machine import Pin
from machine import Timer
from L76GNSS import L76GNSS
from machine import RTC
from network import Sigfox
from network import LoRa
from machine import SD
from LIS2HH12 import LIS2HH12
from network import Bluetooth

import pycom
import utime
import time
import math
import machine
import network
import socket
import struct
import os
import binascii
#tableau de couleurs pour la led
colorled= ['0x007f00', '0x7f7f00', '0x7f0000','0xB805AE','0xE29E32']
#variable pour dÃÂ©finir sur quelle action l'alerte est envoyÃÂ©e
# WAKE_REASON_ACCELEROMETER = 1
# WAKE_REASON_PUSH_BUTTON = 2
# WAKE_REASON_TIMER = 4
# WAKE_REASON_INT_PIN = 8
alarme=2
#notre de cycles avant de donner une coordonnÃÂ©e GPS fictive
cycle_gps_down=10
#numÃÂ©ro de la carte Pycom (BOUSIN)
#sert essentiellement au nom Bluetooth
bousin='BOUSIN 44'
#eteindre la led au dÃÂ©but
pycom.heartbeat(False)
#utilisation des bibliothÃÂ¨ques du Pytrack
py = Pytrack()

rtc = machine.RTC()
utime.sleep_ms(750)
l76 = L76GNSS(py, timeout=30)
chrono = Timer.Chrono()
chrono.start()


#affichage de la raison du rÃÂ©veil
wake_reason=py.get_wake_reason();
print("\n\n Wakeup",  wake_reason)
print(wake_reason)
# enable activity and also inactivity interrupts, using the default callback handler

py.setup_int_wake_up(True, True)
#py.setup_int_pin_wake_up(False)

acc = LIS2HH12()
# enable the activity/inactivity interrupts
# accÃÂ©lÃÂ©ration de 4000mG (4G) pendant une durÃÂ©e de 10s (1000ms)
acc.enable_activity_interrupt(4000, 10000)
volt_float = py.read_battery_voltage()*1000
#construction de la trame pour envoie de la donnÃÂ©e
# TENSION Batterie sur 4
message=int(volt_float)

# check if we were awaken due to activity
#if acc.activity():
#    pycom.rgbled(0xFF0000)
#    message=str(message)+'01ffffffffffffffffff' #alarme BP
#else:
#    pycom.rgbled(0x00FF00)  # timer wake-up
#    message=str(message)+'00ffffffffffffffffff' #evenement horloge
#time.sleep(0.1)

# selon le messgae
if (int(wake_reason) == alarme):
    #activation bluetooth
    print('Activation Bluetooth')
    bt_periph = Bluetooth()
    bt_periph.set_advertisement(name = bousin, service_uuid = b'1234567890123456')
    pycom.rgbled(0x00007f)  # Blue
    pycom.rgbled(0x7f0000) # red
    bt_periph.advertise(True)
    #test de couleurs
for row in colorled:
    print(row)
    pycom.rgbled(int(row))
    utime.sleep_ms(200)

def lora_join():
    # join a network using OTAA (Over the Air Activation)
    lora.join(activation=LoRa.OTAA, auth=(app_eui, app_key), timeout=0)
    #variable pour sortir de la recherche LoRa
    i=0
    while not lora.has_joined():
        if i > 10:
            break
        i+=1
        print('Recherche Objenious...')
        time.sleep(2.5)
    if i > 10:
        print('Objenious introuvable')
        #variable retournee pour savoir si Objenious a ÃÂ©tÃÂ© trouvÃÂ©
        #false = non
        return False
    else:
        print("Objenious est la.")
        #variable retournee pour savoir si Objenious a ÃÂ©tÃÂ© trouvÃÂ©
        #True=oui
        return True


def lora_tx(payload):
    # create a LoRa socket
    lora_socket = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
    # set the LoRaWAN data rate
    lora_socket.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
    # make the socket blocking
    # (waits for the data to be sent and for the 2 receive windows to expire)
    lora_socket.setblocking(True)
    print('Message en cours...')
    lora_socket.send(payload)
    print('Message envoye...')
    # make the socket non-blocking
    # (because if there's no data received it will block forever...)
    lora_socket.setblocking(False)
    lora_socket.close()
##### debut du programme

#volt_float = py.read_battery_voltage()*1000

#construction de la trame pour envoie de la donnÃÂ©e
# TENSION Batterie sur 4
message=int(volt_float)

# selon le messgae
if (int(wake_reason) == alarme):
    message=str(message)+'01ffffffffffffffffff' #alarme BP
else:
    message=str(message)+'00ffffffffffffffffff' #evenement horloge

print(message)

#s.close()

pycom.rgbled(int(colorled[3]))
print('Initialisation LoRa...')
# initialize LoRa in LORAWAN mode, Europe = LoRa.EU868
lora = LoRa(mode=LoRa.LORAWAN, region=LoRa.EU868)
# create an OTAA authentication parameters
app_eui = binascii.unhexlify('70B3D57ED0008CD6')
app_key = binascii.unhexlify('B57F36D88691CEC5EE8659320169A61C')

while True:
    if not lora.has_joined():
        test=lora_join()
    #si la connexion Objenious a ÃÂ©tÃÂ© trouvÃÂ©e
    #on pousse le message
    if test:
        lora_tx(str(message))
    break
print('LoRa FIN...')
# si on est dans le cas d'une alerte
# on coupe le blutooth (plus besoin)
if (int(wake_reason) == alarme):
    bt_periph.deinit()

pycom.rgbled(int(colorled[4]))
print('Connexion a Sigfox')
# init Sigfox for RCZ1 (Europe)
sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)
# create a Sigfox socket
s = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)
# make the socket blocking
s.setblocking(True)
# configure it as uplink only
s.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)
#s.send(bytes([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]))
s.send(bytes([(int(message[0:2],16)),(int(message[2:4],16)), (int(message[4:6],16)),(int(message[6:8],16)),(int(message[8:10],16)),(int(message[10:12],16)),(int(message[12:14],16)),(int(message[14:16],16)),(int(message[16:18],16)),(int(message[18:20],16)),(int(message[20:22],16)),(int(message[22:24],16))]))
s.setblocking(False)
s.close()
print('Message SIGFOX envoye')
pycom.rgbled(int(colorled[2])) # red

#print('Message Sigfox envoye')
## formatage de la trame avec gps
##############################################################################
#si on est dans un cas d'alerte
if (int(wake_reason) == alarme):
    #utime.sleep_ms(20000)
    message=''
    message=int(volt_float)
    message=str(message)+'01' #alarme BP


    ## activation du gps
    ##############################################################################
    last_lon = 0
    last_lat = 0
    min_twitch = 0.000010  ## si modif de la trame GPS
    #variable pour sortir de la recherche GPS
    i=0
    while (True):
        coord = l76.coordinates()  ## Gets coordinates
        if not coord == (None, None):
            lat, lon = coord
            if abs(lat - last_lat) > min_twitch or abs(lon - last_lon) > min_twitch:
                last_lat = lat
                last_lon = lon
            else:
                print("%f,%f" % (lat, lon))
                break
        else:
            #si au bout de cycle_gps_down cycles le GPS n'a pas ÃÂ©tÃÂ© trouvÃÂ©
            #on donne des coordonnÃÂ©es fictives
            if i < cycle_gps_down:
                print("Pas de signal GPS")  #a voir si time out pour la sortie du GPS
                i+=1
            else:
                print("Donnees GPS fictives")  #a voir si time out pour la sortie du GPS
                random=machine.rng()
                random=random*0.00000000001
                lat = 48.81513+random
                lon = 2.268333+random
                print("%f,%f" % (lat, lon))
                break

    pycom.rgbled(int(colorled[0])) # green

    print(message)

    # ernvoie en sigox
    # init Sigfox for RCZ1 (Europe)
    sigfox = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)
    # create a Sigfox socket
    s = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)
    # make the socket blocking
    s.setblocking(True)
    # configure it as uplink only
    s.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)

    #s.send(struct.pack("s", message)+struct.pack("<f", float(lon))+struct.pack("<f", float(lat)))
    s.send( bytes([(int(message[0:2],16)),(int(message[2:4],16)), (int(message[4:6],16))])+struct.pack("<f", float(lon))+struct.pack("<f", float(lat)))
    # fermeture du socket
    s.close()
    # while True:
    #     if not lora.has_joined():
    #         lora_join()
    #     lora_tx(str(message))
    #     break
    # print('LoRa FIN...')
    # pycom.rgbled(int(colorled[4]))


pycom.rgbled(int(colorled[1])) # red


machine.pin_deepsleep_wakeup(['P14'],machine.WAKEUP_ALL_LOW, True)
#dodo
print('Dodo')
#machine.deepsleep(100000*60000)
#py.setup_sleep(40300)
#py.go_to_sleep()
#machine.deepsleep()
# go to sleep for 5 minutes maximum if no accelerometer interrupt happens
time.sleep(1)
py.setup_sleep(86400)
py.go_to_sleep()
